import os
import random
import numpy as np
import networkx as nx
import matplotlib.pyplot as plt

def SIR(G, beta, initial, gamma,desc_dc):
    #SIR传染病模型传播函数.desc_dc是选择的尺度
    print(desc_dc)

    if G.is_directed():
        G.to_undirected()
    y = []
    for i in G.nodes():
        G.nodes[i]['state'] = 'S'
        # 所有人默认为易感染
    s = nx.number_of_nodes(G) - initial  # 易感染人数

    ps = nx.random_layout(G)  # 布置框架
    colors = {"R": 'b', "I": 'r', "S": 'g'}
    states = nx.get_node_attributes(G, 'state')  # 获得节点的state属性
    color = [colors[states[i]] for i in G.nodes()]
    nx.draw(G, ps, node_color=color, with_labels=True, node_size=300)
    plt.show()
    #起初大家都是易感染的


    i_nodes = []
    # 选择前initial个xxx最高的节点设为感染源（可以是度中心性、混合影响力指标等等）
    for i in range(initial):
        G.nodes[desc_dc[i][0]]['state'] = 'I'
        i_nodes.append(desc_dc[i][0])
    y.append(((s, (len(i_nodes)), 0)))
    # 开始传播，直到所有人被传染

    r_nodes = nx.Graph()
    round = 0
    while len(i_nodes) != 0:
        round += 1
        print("round:",round)
        print(s)

        for i in i_nodes:
            if random.random() < gamma:
                # 当前恢复人数 gamma 概率
                r_nodes.add_node(i)
                i_nodes.remove(i)
                G.nodes[i]['state'] = 'R'

        for i in i_nodes:
            # 按beta概率传染I节点的邻居节点
            for node in nx.all_neighbors(G,i):
                r = random.random()
                if r < beta and G.nodes[node]['state'] == 'S':
                    G.nodes[node]['state'] = 'I'
                    if node not in i_nodes:
                        i_nodes.append(node)
                    #传染成功
        print("infected:",len(i_nodes))
        s = nx.number_of_nodes(G) - len(i_nodes) - len(r_nodes.nodes)
        i = len(i_nodes)
        r = len(r_nodes.nodes)

        y.append((s, i, r))
        states = nx.get_node_attributes(G, 'state')  # 获得节点当前状态
        color = [colors[states[i]] for i in G.nodes()]
        nx.draw(G, ps, node_color=color, with_labels=True, node_size=100)
        plt.show()
    return np.array(y)