import networkx as nx

import matplotlib.pyplot as plt

BA= nx.random_graphs.barabasi_albert_graph(30,3)  #生成n=30、m=2的BA无标度网络

pos = nx.spring_layout(BA)          #定义一个布局，此处采用了spring布局方式

plt.figure(figsize=(10,10))

d=dict(BA.degree)

nx.draw(BA,pos,with_labels=True,node_size = [v*150 for v in d.values()])  #绘制图形

foo_fig = plt.gcf() # 'get current figure'
foo_fig.savefig('BA_30nodes.eps', format='eps', dpi=1000)

plt.show()