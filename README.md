# Top-k influence node acquisition in a complex network model

##### 介绍  introduction
Find top-k influence nodes based on hybrid algorithms and improved k-shell decomposition algorithm

本方法是基于k-shell方法提出的一种在分解过程中使用了迭代信息的*多属性k-shell*方法。该方法首先结合了sigmod函数和迭代信息来得到位置指数，之后再次结合位置指数和k-shell中的shell值得到位置属性（position attribute）。其次，通过节点的局部信息得到邻居属性（neighbor property）。最后，使用信息熵加权法给位置属性以及邻居属性进行加权。

